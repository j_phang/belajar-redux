import {createStore, applyMiddleware} from 'redux';
import rootReducer from './src/redux/reducer/index';
import createSagaMiddleware from 'redux-saga';
import IndexSaga from './src/redux/saga/index';
// Deklarasi Pembuatan Middleware menggunakan SAGA
const sagaMiddleware = createSagaMiddleware();

const configureStore = {
  // Meng-inject Middleware ke dalam Redux
  ...createStore(rootReducer, applyMiddleware(sagaMiddleware)),
  // Mendeklarasikan Lokasi Saga yang akan dijalankan
  runSaga: sagaMiddleware.run(IndexSaga),
};

export default configureStore;
