const initialState = {
  token: 'Contoh Default',
  username: '',
  fullName: '',
};

export default (state = initialState, action) => {
  // if (action.type === 'SET_AUTH') {
  //   return {
  //     ...state,
  //     token: action.payload
  //   }
  // } else if (action.type === 'LOGOUT') {
  //   return {
  //     ...state,
  //     token: 'Default'
  //   }
  // } else {
  //   return state;
  // }

  switch (action.type) {
    case 'SET_AUTH':
      return {
        ...state,
        token: action.payload,
      };
    case 'LOGOUT':
      return {
        ...state, //Isi state ini kurang lebih gini
        // username: '',
        // fullName: '',
        token: action.payload,
      };
    default:
      return state;
  }
};
