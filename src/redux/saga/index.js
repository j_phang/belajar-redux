import {takeLatest, all} from '@redux-saga/core/effects';
import {getUserData} from './user';

export default function* IndexSaga() {
  yield all([
    takeLatest('LOGIN', getUserData),
    takeLatest('GET_NEWS', getUserData),
    takeLatest('GET_PROFILE', getUserData),
    takeLatest('GET_COMMENTS', getUserData),
  ]);
}
