import 'react-native-gesture-handler';
import * as React from 'react';
// Navigator
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// Redux
import {Provider} from 'react-redux';
import store from './store';
// Screen
import Home from './src/screen/Home';
import Login from './src/screen/Login';

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Register" component={Home} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Dashboard" component={Home} />
          <Stack.Screen name="Profile" component={Home} />
          <Stack.Screen name="Details" component={Home} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
