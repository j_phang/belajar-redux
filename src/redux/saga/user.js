import {put} from 'redux-saga/effects';
import {apiGetUser} from './api/apiUser';
// {username: '...', password: '...'}

export function* getUserData(data) {
  try {
    console.log(data);
    // data = {type: 'LOGIN', payload: {username: '', password: ''}};
    const token = yield apiGetUser(data.payload);
    console.log(token);
    yield put({type: 'SET_AUTH', payload: token});
  } catch (error) {
    console.log(error);
  }
}
