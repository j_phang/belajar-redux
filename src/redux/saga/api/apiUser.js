import axios from 'axios';

export function* apiGetUser(data) {
  // data = { username: '', password: '' }
  const response = yield axios.post(
    'https://backendtodo101.herokuapp.com/user/login',
    data,
  );
  console.log(response);
  return response.data.token;
}
