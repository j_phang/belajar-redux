import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import {setAuth} from '../redux/action/auth';

function Home({navigation, token, login}) {
  const callRedux = () => {
    login({username: 'email@mail.com', password: '@Pass123'});
  };

  return (
    <>
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Login')}>
              <Text>Touch Me And Move to Login!</Text>
            </TouchableOpacity>
            <Text>Ini adalah Props dari Redux :: {token}</Text>
            <TouchableOpacity onPress={() => callRedux()}>
              <Text>Login</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({});

const mapStateToProps = (state) => ({
  token: state.auth.token,
});

const mapDispatchToProps = (dispatch) => {
  return {
    login: (data) => dispatch(setAuth(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
